use crate::models::{ArticleID, ArticleOrder, CategoryID, FeedID, Marked, Read, TagID};
use chrono::NaiveDateTime;

#[derive(Clone, Debug)]
pub struct ArticleFilter<'a> {
    pub limit: Option<i64>,
    pub offset: Option<i64>,
    pub order: Option<ArticleOrder>,
    pub unread: Option<Read>,
    pub marked: Option<Marked>,
    pub feed: Option<FeedID>,
    pub feed_blacklist: Option<Vec<FeedID>>,
    pub category: Option<CategoryID>,
    pub category_blacklist: Option<Vec<CategoryID>>,
    pub tag: Option<TagID>,
    pub ids: Option<&'a [ArticleID]>,
    pub newer_than: Option<NaiveDateTime>,
    pub older_than: Option<NaiveDateTime>,
    pub search_term: Option<String>,
}
