use crate::models::{FeedID, Url};
use crate::schema::fav_icons;
use chrono::{NaiveDateTime, Utc};

#[derive(Identifiable, Queryable, Clone, Debug, Insertable, Eq)]
#[primary_key(feed_id)]
#[table_name = "fav_icons"]
pub struct FavIcon {
    pub feed_id: FeedID,
    #[column_name = "timestamp"]
    pub expires: NaiveDateTime,
    pub format: Option<String>,
    pub etag: Option<String>,
    pub source_url: Option<Url>,
    pub data: Option<Vec<u8>>,
}

impl PartialEq for FavIcon {
    fn eq(&self, other: &FavIcon) -> bool {
        self.feed_id == other.feed_id
    }
}

impl FavIcon {
    pub fn is_expired(&self) -> bool {
        Utc::now().naive_utc() >= self.expires
    }
}
