use super::LocalRSS;
use crate::feed_api::{ApiMetadata, FeedApi, FeedApiErrorKind, FeedApiResult, Portal};
use crate::models::{LoginGUI, PluginID, PluginIcon, PluginInfo, ServiceLicense, ServicePrice, ServiceType, VectorIcon};
use failure::Fail;
use rust_embed::RustEmbed;
use std::path::PathBuf;
use std::str;

#[derive(RustEmbed)]
#[folder = "src/feed_api_implementations/local"]
struct LocalResources;

pub struct LocalMetadata;

impl LocalMetadata {
    pub fn get_id() -> PluginID {
        PluginID::new("local_rss")
    }
}

impl ApiMetadata for LocalMetadata {
    fn id(&self) -> PluginID {
        Self::get_id()
    }

    fn info(&self) -> FeedApiResult<PluginInfo> {
        let icon_data = LocalResources::get("feed-service-local.svg").ok_or(FeedApiErrorKind::Resource)?;
        let icon = VectorIcon {
            data: icon_data.to_vec(),
            width: 48,
            height: 48,
        };
        let icon = PluginIcon::Vector(icon);

        let symbolic_icon_data = LocalResources::get("feed-service-local-symbolic.svg").ok_or(FeedApiErrorKind::Resource)?;
        let symbolic_icon = VectorIcon {
            data: symbolic_icon_data.to_vec(),
            width: 48,
            height: 48,
        };
        let symbolic_icon = PluginIcon::Vector(symbolic_icon);

        let login_gui = LoginGUI::None;

        Ok(PluginInfo {
            id: self.id(),
            name: String::from("Local RSS"),
            icon: Some(icon),
            icon_symbolic: Some(symbolic_icon),
            website: None,
            service_type: ServiceType::Local,
            license_type: ServiceLicense::GPlv3,
            service_price: ServicePrice::Free,
            login_gui,
        })
    }

    fn parse_error(&self, _error: &dyn Fail) -> Option<String> {
        None
    }

    fn get_instance(&self, _path: &PathBuf, portal: Box<dyn Portal>) -> FeedApiResult<Box<dyn FeedApi>> {
        let local_rss = LocalRSS { portal };
        let local_rss = Box::new(local_rss);
        Ok(local_rss)
    }
}
