pub mod feedbin;
pub mod feedly;
pub mod fever;
pub mod local;
pub mod miniflux;

use self::feedbin::metadata::FeedbinMetadata;
use self::feedly::metadata::FeedlyMetadata;
use self::fever::metadata::FeverMetadata;
use self::local::metadata::LocalMetadata;
use self::miniflux::metadata::MinifluxMetadata;
use crate::feed_api::ApiMetadata;
use crate::models::PluginID;

pub struct FeedApiImplementations;

impl FeedApiImplementations {
    pub fn list() -> Vec<Box<dyn ApiMetadata>> {
        let mut h: Vec<Box<dyn ApiMetadata>> = Vec::new();

        h.push(Box::new(FeedbinMetadata));
        h.push(Box::new(FeedlyMetadata));
        h.push(Box::new(FeverMetadata));
        h.push(Box::new(MinifluxMetadata));
        h.push(Box::new(LocalMetadata));

        h
    }

    pub fn get(id: &PluginID) -> Option<Box<dyn ApiMetadata>> {
        let list = Self::list();
        for api_meta in list {
            if &api_meta.id() == id {
                return Some(api_meta);
            }
        }

        None
    }
}
