table! {
    articles (article_id) {
        article_id -> Text,
        title -> Nullable<Text>,
        author -> Nullable<Text>,
        feed_id -> Text,
        url -> Nullable<Text>,
        timestamp -> Timestamp,
        html -> Nullable<Text>,
        summary -> Nullable<Text>,
        direction -> Nullable<Integer>,
        unread -> Integer,
        marked -> Integer,
    }
}

table! {
    categories (category_id) {
        category_id -> Text,
        label -> Text,
        parent_id -> Text,
        sort_index -> Nullable<Integer>,
        category_type -> Integer,
    }
}

table! {
    enclosures (article_id) {
        article_id -> Text,
        url -> Text,
        enclosure_type -> Integer,
    }
}

table! {
    fav_icons (feed_id) {
        feed_id -> Text,
        timestamp -> Timestamp,
        format -> Nullable<Text>,
        etag -> Nullable<Text>,
        source_url -> Nullable<Text>,
        data -> Nullable<Binary>,
    }
}

table! {
    feed_mapping (feed_id, category_id) {
        feed_id -> Text,
        category_id -> Text,
    }
}

table! {
    feeds (feed_id) {
        feed_id -> Text,
        label -> Text,
        website -> Nullable<Text>,
        feed_url -> Nullable<Text>,
        icon_url -> Nullable<Text>,
        sort_index -> Nullable<Integer>,
    }
}

table! {
    taggings (article_id, tag_id) {
        article_id -> Text,
        tag_id -> Text,
    }
}

table! {
    tags (tag_id) {
        tag_id -> Text,
        label -> Text,
        color -> Nullable<Text>,
        sort_index -> Nullable<Integer>,
    }
}

joinable!(articles -> feeds (feed_id));
joinable!(enclosures -> articles (article_id));
joinable!(fav_icons -> feeds (feed_id));
joinable!(feed_mapping -> categories (category_id));
joinable!(feed_mapping -> feeds (feed_id));
joinable!(taggings -> articles (article_id));
joinable!(taggings -> tags (tag_id));

allow_tables_to_appear_in_same_query!(articles, categories, enclosures, fav_icons, feed_mapping, feeds, taggings, tags,);
