use std::env;
use std::fs::File;
use std::io::Write;
use std::path::Path;

fn main() {
    // setup feedly API secrets
    let feedly_client_id = match env::var("FEEDLY_CLIENT_ID") {
        Ok(client_id) => client_id,
        Err(_) => {
            println!("FEEDLY_CLIENT_ID not defined");
            "FEEDLY_CLIENT_ID_FALLBACK".to_owned()
        }
    };

    let feedly_client_secret = match env::var("FEEDLY_CLIENT_SECRET") {
        Ok(client_secret) => client_secret,
        Err(_) => {
            println!("FEEDLY_CLIENT_SECRET not defined");
            "FEEDLY_CLIENT_SECRET_FALLBACK".to_owned()
        }
    };

    let feedly_dest_path = Path::new("src/feed_api_implementations/feedly/secrets.json");

    let mut f = File::create(&feedly_dest_path).expect("Cant create feedly secrets file");

    let feedly_secrets = format!(
        "{{\n    \"client_id\": \"{}\",\n    \"client_secret\": \"{}\"\n}}",
        feedly_client_id, feedly_client_secret
    );
    f.write_all(&feedly_secrets.as_bytes()).expect("failed to write feedly secrets.json");

    // setup password encryption key
    let password_crypt_key = match env::var("PASSWORD_CRYPT_KEY") {
        Ok(pass_crypt_key) => pass_crypt_key,
        Err(_) => {
            println!("PASSWORD_CRYPT_KEY not defined");
            "PASSWORD_CRYPT_KEY_FALLBACK".to_owned()
        }
    };

    let password_dest_path = Path::new("./resources/password_crypt_key.json");

    let resource_folder = password_dest_path.parent().expect("Can't extract parent folder from path");

    std::fs::create_dir_all(resource_folder).expect("couldn't create resource folder");

    let mut f = File::create(&password_dest_path).expect("Cant create password crypt file");

    let password_secrets = format!("{{\n    \"password_crypt_key\": \"{}\"\n}}", password_crypt_key);
    f.write_all(&password_secrets.as_bytes())
        .expect("failed to write password_crypt_key.json");
}
